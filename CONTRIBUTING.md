# Contributing to Assistant

Assistant is a complex project. It is not perfect but aims to be perfectible.

You want to improve it? Here is how.

## Sharing is caring

The easiest way to help the project is to share it with anyone who might be interested. Spread the word about Assistant. The more people use it, the better it gets. What are you waiting for?

## Bug bounty

While using Assistant or its related technologies, you might stumble upon a bug of some sort. Whether it be a minor annoyance or serious security flaw, you can always [open a ticket](https://gitlab.com/waser-technologies/technologies/assistant/-/issues/new) to tell us what went wrong and how to improve it.

## Code contributions

If you have the time and skills, we always enjoy to review your implementations. Just open a pull request on the appropriate repository and share your contributions for everyone to enjoy. From fixing open issues or proposing new features, nothing is off the table when it comes to improving Assistant.

## Financial contributions

If for whatever reason, the above options are not suitable for you, feel free to help the project financially. Whether it be a onetime donation or a recurrent one, any donation no matter the amount is always appreciated.

For now, we only accept donations in either Bitcoin or ERC-20 tokens such as Ether.

> ERC-20: 0x7c9fFAfBC8ACb358fa823B7EC72Bf1C211AB6994

With ERC-20, you can leave a message to Assistant. We'll add the most popular types of queries to the appropriate domains so that Assistant can answer them.

> BTC: bc1qfd07dude5r95kgzcjkyujykketuxzlf0xh9n3d

If you prefer to use cash, you can also become a sponsor thru the [GitHub' Sponsor Program](https://github.com/sponsors/wasertech) and recive exclusive rewards.

If those means are not suitable for you, [send me an email](mailto:danny@waser.tech), I'm sure we'll be able to find a way that suits everyone.

### How is Assistant going to use those funds ?

Transparency is important for this project to succeed. So will be completly transparent with those funds.

Most of them are going to be used to enable talented people to spend more time working to improve Assistant. Rewarding our most active contributors is the best way to keep them engaged in the project and ensure the project's future.

We'll also allocate resources to rent places for those talents to work in ideal conditions.

Investing in the futur is also a priority, meaning acquiring more computing power and energy for Assistant to compute.

Marketing is not really a priority but it's important for every project success so we'll also setup a budget for promotion.

We do things right or we don't do it at all so legal council and the creation of an open-source software foundation is the first milestone.
