### TODOs
| Filename | line # | TODO |
|:------|:------:|:------|
| [assistant/main.py](assistant/main.py#L326) | 326 | use AssistantExecer to load custom builtin commands |
| [assistant/main.py](assistant/main.py#L537) | 537 | Enable NLP inside script files |
| [assistant/nlp/chains/docs_explained.py](assistant/nlp/chains/docs_explained.py#L49) | 49 | Try multiple LLMs and pick the first one that answered. |
| [assistant/nlp/chains/tools/search.py](assistant/nlp/chains/tools/search.py#L20) | 20 | get answer here and print it and return it instead. |
| [assistant/nlp/chains/tools/search.py](assistant/nlp/chains/tools/search.py#L29) | 29 | get answer here and print it and return it instead. |
| [assistant/nlp/chains/tools/wikipedia.py](assistant/nlp/chains/tools/wikipedia.py#L29) | 29 | get answer here and print it and return it instead. |
| [assistant/nlp/chains/tools/wikipedia.py](assistant/nlp/chains/tools/wikipedia.py#L35) | 35 | get answer here and print it and return it instead. |
