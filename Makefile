.PHONY: todo

PYTHON_FILES := $(shell find assistant -name '*.py')

test:
	@python -m unittest discover -s tests -p 'test_*.py'
	@python -m pytest tests

release:
	@python -m build || echo "Build failed: install build tools with 'pip install build'"
	@python -m twine upload dist/* || echo "Upload failed: install twine with 'pip install twine'"
	@rm dist/* || echo "Failed to remove dist files"

TODO.md: $(PYTHON_FILES)
	@npx leasot --reporter markdown $^ > $@ || true
