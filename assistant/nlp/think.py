let_me_think_about_it =  [
    "Thinking",
    "Let me think about it",
    "Please wait while I think about it",
    "I need some time to think about it",
    "I'm thinking about it, please be patient",
    "Give me a moment to think about it",
    "I'm processing your request, please hold on",
    "Considering, please stand by",
    "I'm pondering the sens of your words, I thank you for your consideration",
    "I'm thinking about it, please wait",
    "I'm reflecting on it, please wait",
    "I'm wondering",
    "Let me see",
    "Let me see what I can do",
]

sorry_unable_to_think = [
    "Sorry, I am unable to reach the language model.",
    "My apologizes but it seems that I cannot reach the language model at this very moment.",
    "I'm sorry, I cannot reach the language model.",
    "The language model is not available at this time.",
    "Unfortunately, I cannot reach the language model.",
    "Unreachable language model, please excuse this inconvenience.",
    "My deepest apologizes, but I cannot reach the language model.",
]