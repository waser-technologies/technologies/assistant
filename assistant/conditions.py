#from assistant.environ import ASSISTANT_NLP_HOST, ASSISTANT_NLP_PORT

def is_nlp_server_up(nlp):
    return nlp.is_nlp_server_up()
